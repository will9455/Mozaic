//
//  ViewController.swift
//  Mozaic Keys
//
//  Created by William Ravenscroft on 14/11/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//

import UIKit

class KeysController: UIViewController {
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var keyboardStackView: UIStackView!
    
    var pianoController = PianoController()
    var touchAmplitude = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
   
    @IBAction func playTonic(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(button1.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
            print(touchAmplitude)
        }
        pianoController.playNote(midi: 60, amplitude: touchAmplitude)
        //print("piano", pianoController.piano.isStarted)
        print("mixer", pianoController.mixer.isStarted)
    }
    @IBAction func playSuperTonic(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(button1.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
            print(touchAmplitude)
        }
        pianoController.playNote(midi: 70, amplitude: touchAmplitude)
        //print("piano", pianoController.piano.isStarted)
        print("mixer", pianoController.mixer.isStarted)
        print("Audio kit is running", pianoController.AudioKitOutput)
        
    }
    
}

