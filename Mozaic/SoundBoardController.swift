//
//  SoundBoardController.swift
//  Mozaic
//
//  Created by William Ravenscroft on 12/10/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//

import UIKit
import AudioKit

class SoundBoardController: UIViewController {
    
    // FX Control Panel Controls
    struct FXControlPanel {
        var FXParam1Knob: MZKnob!
        var FXParam2Knob: MZKnob!
        var FXDWSlider: UISlider!
    }
    
    var soundBoard = SoundBoard()
    
    // Knob Placeholders
    @IBOutlet weak var OutputKnobPlaceholder: UIView!
    
    @IBOutlet weak var OSCKnobOnePlaceholder: UIView!
    @IBOutlet weak var OSCKnobTwoPlaceholder: UIView!
    @IBOutlet weak var OSCKnobThreePlaceholder: UIView!
    
    @IBOutlet weak var F1CutoffPlaceholder: UIView!
    @IBOutlet weak var F1ResonancePlaceholder: UIView!
    @IBOutlet weak var F1DWPlaceholder: UIView!
    
    @IBOutlet weak var F2CutoffPlaceholder: UIView!
    @IBOutlet weak var F2ResonancePlaceholder: UIView!
    @IBOutlet weak var F2DWPlaceholder: UIView!
    
    @IBOutlet weak var FX1Param1Placeholder: UIView!
    @IBOutlet weak var FX1Param2Placeholder: UIView!
    
    @IBOutlet weak var FX2Param1Placeholder: UIView!
    @IBOutlet weak var FX2Param2Placeholder: UIView!
    
    @IBOutlet weak var DistortionDrivePlaceholder: UIView!
    @IBOutlet weak var DistortionDWPlaceholder: UIView!
    
    @IBOutlet weak var LFO1RatePlaceholder: UIView!
    @IBOutlet weak var LFO1AmountPlaceholder: UIView!
    @IBOutlet weak var LFO2RatePlaceholder: UIView!
    @IBOutlet weak var LFO2AmountPlaceholder: UIView!
    
    @IBOutlet weak var VibratoPlaceholder: UIView!
    @IBOutlet weak var PanPlaceholder: UIView!
    @IBOutlet weak var DecimatorPlaceholder: UIView!
    
    // Distortion Signal Switch
    @IBOutlet weak var DistortionSegmentedControl: UISegmentedControl!
    
    // Voice Control Panel
    @IBOutlet weak var VoiceStepper: UIStepper!
    @IBOutlet weak var VoiceSpreadSlider: UISlider!
    @IBOutlet weak var VoiceNumberLabel: UILabel!
    
    // FX Slider Outlets
    @IBOutlet weak var FX1DWSlider: UISlider!
    @IBOutlet weak var FX2DWSlider: UISlider!
    
    // Knobs & Arrays (Control Panels)
    var OutputKnob: MZKnob!
    var DistortionDriveKnob: MZKnob!
    var DistortionDWKnob: MZKnob!
    var DecimatorKnob: MZKnob!
    var PanKnob: MZKnob!
    var VibratoKnob: MZKnob!
    var OSCKnobs: [MZKnob]!
    var F1Knobs: [MZKnob]!
    var F2Knobs: [MZKnob]!
    var LFO1Knobs: [MZKnob]!
    var LFO2Knobs: [MZKnob]!
    
    // FX Controls
    var FX1Panel: FXControlPanel!
    var FX2Panel: FXControlPanel!
    
    // Knob Placeholders & Arrays
    var OSCKnobPlaceholders: [UIView]!
    var F1KnobPlaceholders: [UIView]!
    var F2KnobPlaceholders: [UIView]!
    var LFO1KnobPlaceholders: [UIView]!
    var LFO2KnobPlaceholders: [UIView]!
    
    // Vibrato slider
    @IBOutlet weak var vibratoAmount: VerticalSlider!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        vibratoAmount.slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        // Do any additional setup after loading the view, typically from a nib.
        createControlPanels()
        soundBoard.startEngine()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /** Function to create UI knobs for controls **/
    fileprivate func createControlPanels() {
        
        // OSC Knobs
        OSCKnobPlaceholders = [OSCKnobOnePlaceholder, OSCKnobTwoPlaceholder, OSCKnobThreePlaceholder]
        
        let OSCKnobOne = MZKnob(frame: OSCKnobOnePlaceholder.bounds)
        let OSCKnobTwo = MZKnob(frame: OSCKnobTwoPlaceholder.bounds)
        let OSCKnobThree = MZKnob(frame: OSCKnobThreePlaceholder.bounds)
        
        OSCKnobs = [OSCKnobOne, OSCKnobTwo, OSCKnobThree]
        
        for i in 0...OSCKnobPlaceholders.count-1 {
            OSCKnobs[i].lineWidth = 4.0
            OSCKnobs[i].pointerLength = 30.0
            OSCKnobs[i].setValue(value: 0, animated: true)
            OSCKnobPlaceholders[i].addSubview(OSCKnobs[i])
        }
        
        // Output Knob
        OutputKnob = MZKnob(frame: OutputKnobPlaceholder.bounds)
        OutputKnob.lineWidth = 4.0
        OutputKnob.pointerLength = 30.0
        OutputKnob.setValue(value: 0, animated: true)
        OutputKnobPlaceholder.addSubview(OutputKnob)
        
        // Filter Knobs
        F1KnobPlaceholders = [F1CutoffPlaceholder, F1ResonancePlaceholder, F1DWPlaceholder]
        F2KnobPlaceholders = [F2CutoffPlaceholder, F2ResonancePlaceholder, F2DWPlaceholder]
        
        // Filter 1
        let F1CutoffKnob = MZKnob(frame: F1CutoffPlaceholder.bounds)
        let F1ResonanceKnob = MZKnob(frame: F1ResonancePlaceholder.bounds)
        let F1DWKnob = MZKnob(frame: F1DWPlaceholder.bounds)
        
        F1Knobs = [F1CutoffKnob, F1ResonanceKnob, F1DWKnob]
        
        // Filter 2
        let F2CutoffKnob = MZKnob(frame: F2CutoffPlaceholder.bounds)
        let F2ResonanceKnob = MZKnob(frame: F2ResonancePlaceholder.bounds)
        let F2DWKnob = MZKnob(frame: F2DWPlaceholder.bounds)
        
        F2Knobs = [F2CutoffKnob, F2ResonanceKnob, F2DWKnob]
        
        // Add filter knobs to placeholders
        for i in 0...F1KnobPlaceholders.count-1 {
            F1Knobs[i].lineWidth = 4.0
            F1Knobs[i].pointerLength = 30.0
            F1Knobs[i].setValue(value: 0, animated: true)
            F1KnobPlaceholders[i].addSubview(F1Knobs[i])
            
            F2Knobs[i].lineWidth = 4.0
            F2Knobs[i].pointerLength = 30.0
            F2Knobs[i].setValue(value: 0, animated: true)
            F2KnobPlaceholders[i].addSubview(F2Knobs[i])
        }
        
        // FX Panel Control Structures
        FX1Panel = FXControlPanel(FXParam1Knob: MZKnob(frame: FX1Param1Placeholder.bounds), FXParam2Knob: MZKnob(frame: FX1Param2Placeholder.bounds), FXDWSlider:  FX1DWSlider)
        FX2Panel = FXControlPanel(FXParam1Knob: MZKnob(frame: FX2Param1Placeholder.bounds), FXParam2Knob: MZKnob(frame: FX2Param2Placeholder.bounds), FXDWSlider:  FX2DWSlider)
        
        FX1Panel.FXParam1Knob = MZKnob(frame: FX1Param1Placeholder.bounds)
        FX1Panel.FXParam2Knob = MZKnob(frame: FX1Param2Placeholder.bounds)
        FX1Panel.FXDWSlider = FX1DWSlider
        
        FX2Panel.FXParam1Knob = MZKnob(frame: FX2Param1Placeholder.bounds)
        FX2Panel.FXParam2Knob = MZKnob(frame: FX2Param2Placeholder.bounds)
        FX2Panel.FXDWSlider = FX2DWSlider
        
        FX1Panel.FXParam1Knob.lineWidth = 4.0
        FX1Panel.FXParam1Knob.pointerLength = 30.0
        FX1Panel.FXParam1Knob.setValue(value: 0, animated: true)
        FX1Panel.FXParam2Knob.lineWidth = 4.0
        FX1Panel.FXParam2Knob.pointerLength = 30.0
        FX1Panel.FXParam2Knob.setValue(value: 0, animated: true)
        
        FX2Panel.FXParam1Knob.lineWidth = 4.0
        FX2Panel.FXParam1Knob.pointerLength = 30.0
        FX2Panel.FXParam1Knob.setValue(value: 0, animated: true)
        FX2Panel.FXParam2Knob.lineWidth = 4.0
        FX2Panel.FXParam2Knob.pointerLength = 30.0
        FX2Panel.FXParam2Knob.setValue(value: 0, animated: true)
        
        FX1Param1Placeholder.addSubview(FX1Panel.FXParam1Knob)
        FX1Param2Placeholder.addSubview(FX1Panel.FXParam2Knob)
        FX2Param1Placeholder.addSubview(FX2Panel.FXParam1Knob)
        FX2Param2Placeholder.addSubview(FX2Panel.FXParam2Knob)
        
        let LFO1RateKnob = MZKnob(frame: LFO1RatePlaceholder.bounds)
        let LFO1AmountKnob = MZKnob(frame: LFO1AmountPlaceholder.bounds)
        let LFO2RateKnob = MZKnob(frame: LFO2RatePlaceholder.bounds)
        let LFO2AmountKnob = MZKnob(frame: LFO2AmountPlaceholder.bounds)
     
        LFO1Knobs = [LFO1RateKnob, LFO1AmountKnob]
        LFO2Knobs = [LFO2RateKnob, LFO2AmountKnob]
        
        LFO1KnobPlaceholders = [LFO1RatePlaceholder, LFO1AmountPlaceholder]
        LFO2KnobPlaceholders = [LFO2RatePlaceholder, LFO2AmountPlaceholder]
        
        for i in 0...LFO1Knobs.count-1 {
            LFO1Knobs[i].lineWidth = 4.0
            LFO1Knobs[i].pointerLength = 30.0
            LFO1Knobs[i].setValue(value: 0, animated: true)
            LFO1KnobPlaceholders[i].addSubview(LFO1Knobs[i])
            
            LFO2Knobs[i].lineWidth = 4.0
            LFO2Knobs[i].pointerLength = 30.0
            LFO2Knobs[i].setValue(value: 0, animated: true)
            LFO2KnobPlaceholders[i].addSubview(LFO2Knobs[i])
        }
        
        // Distortion Controls
        DistortionDriveKnob = MZKnob(frame: DistortionDrivePlaceholder.bounds)
        DistortionDriveKnob.lineWidth = 4.0
        DistortionDriveKnob.pointerLength = 30.0
        DistortionDriveKnob.setValue(value: 0, animated: true)
        DistortionDrivePlaceholder.addSubview(DistortionDriveKnob)
        
        DistortionDWKnob = MZKnob(frame: DistortionDWPlaceholder.bounds)
        DistortionDWKnob.lineWidth = 4.0
        DistortionDWKnob.pointerLength = 30.0
        DistortionDWKnob.setValue(value: 0, animated: true)
        DistortionDWPlaceholder.addSubview(DistortionDWKnob)
        
        DecimatorKnob = MZKnob(frame: DecimatorPlaceholder.bounds)
        DecimatorKnob.lineWidth = 4.0
        DecimatorKnob.pointerLength = 30.0
        DecimatorKnob.setValue(value: 0, animated: true)
        DecimatorPlaceholder.addSubview(DecimatorKnob)
        
        PanKnob = MZKnob(frame: PanPlaceholder.bounds)
        PanKnob.lineWidth = 4.0
        PanKnob.pointerLength = 30.0
        PanKnob.setValue(value: 0, animated: true)
        PanPlaceholder.addSubview(PanKnob)
        
        VibratoKnob = MZKnob(frame: VibratoPlaceholder.bounds)
        VibratoKnob.lineWidth = 4.0
        VibratoKnob.pointerLength = 30.0
        VibratoKnob.setValue(value: 0, animated: true)
        VibratoPlaceholder.addSubview(VibratoKnob)
    }
    
    @objc func sliderChanged() {
        if vibratoAmount.value == vibratoAmount.maximumValue {
            vibratoAmount.maximumTrackTintColor = UIColor.init(red: 246/256, green: 112/256, blue: 0, alpha: 1.0)
        } else if vibratoAmount.value == vibratoAmount.minimumValue {
            vibratoAmount.minimumTrackTintColor = UIColor.init(red: 104/256, green: 104/256, blue: 104/256, alpha: 1.0)
        } else {
            vibratoAmount.maximumTrackTintColor = UIColor.init(red: 104/256, green: 104/256, blue: 104/256, alpha: 1.0)
            vibratoAmount.minimumTrackTintColor = UIColor.init(red: 246/256, green: 112/256, blue: 0, alpha: 1.0)
        }
    }
    
   
    @IBAction func playButtonOne(_ sender: Any) {
        soundBoard.playNote(note: MIDINoteNumber(60))
    }
    
    @IBAction func stopButtonOne(_ sender: Any) {
        soundBoard.stopNote(note: MIDINoteNumber(60))
    }
    
}
