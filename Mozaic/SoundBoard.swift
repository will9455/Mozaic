//
//  SoundBoard.swift
//  Mozaic
//
//  Created by William Ravenscroft on 16/10/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//

import Foundation
import AudioKit

class SoundBoard {
    
    struct preset {
        var voiceNumber = 1
        var voiceSpread = 0.0
        var oscillatorWaveform = [0.0, 0.0, 0.0]
        var oscillatorAttack = [0.5, 0.2, 0.2]
        var oscillatorDecay = [0.5, 0.5, 0.5]
        var oscillatorSustain = [0.7, 0.7, 0.7]
        var oscillatorRelease = [0.2, 0.2, 0.2]
        var oscillatorDetune = [0.0, 0.0, 0.0]
        var oscillatorOctave = [MIDINoteNumber(0), MIDINoteNumber(0), MIDINoteNumber(0)]
        var oscillatorVolume = [0.7, 0.0, 0.0]
        var oscillatorFilterMix = [0.5, 0.5, 0.5]
        var filterType = ["lpf", "hpf"] // Other types: bpf, brf
        var filterState = [false, false] // true = on
        var filterCutoff = [0.5, 0.5]
        var filterResonance = [0.2, 0.2]
        var filterDryWet = [1.0, 1.0]
        var fxType = ["phaser", "reverb"] // Other types "delay" "flanger"
        var fxState = [false, false]
        var fxParam1 = [0.3, 0.3]
        var fxParam2 = [0.3, 0.3]
        var distortionState = false
        var distortionDrive = 0.3
        var distortionDryWet = 0.5
        var distortionPrePost = 1
        var lfoAssignment = ["f1cutoff", "f2bandwidth"]
        var lfoRate = [0.3, 0.3]
        var lfoAmount = [0.0, 0.0]
        var decimator = 0.0
        var vibratoDepth = 0.0
        var vibratoRate = 0.0
        var pan = 0.5
        var master = 0.5
    }
    
    let initPreset = preset.init()
    var currentSettings = preset.init()
    var oscillatorArray = Array(repeating: [AKMorphingOscillatorBank](), count: 3)
    var oscillatorMixers = [AKMixer]()
    var oscillatorBankMixer = AKMixer()
    var voiceMixer = AKMixer()
    var OSC1Mixer = AKMixer()
    var OSC2Mixer = AKMixer()
    var OSC3Mixer = AKMixer()
    var OSC1VoiceArray = [AKMorphingOscillatorBank]()
    var OSC2VoiceArray = [AKMorphingOscillatorBank]()
    var OSC3VoiceArray = [AKMorphingOscillatorBank]()
    var preFilterMixers = [AKMixer(), AKMixer()]
    // Filters
    var filterNodes = [AKNode(), AKNode()]
    var hpFilters = [AKHighPassFilter(), AKHighPassFilter()]
    var lpFilters = [AKLowPassFilter(), AKLowPassFilter()]
    var bpFilters = [AKBandPassButterworthFilter(), AKBandPassButterworthFilter()]
    var bpDWMixers = [AKDryWetMixer(), AKDryWetMixer()]
    var brFilters = [AKBandRejectButterworthFilter(), AKBandRejectButterworthFilter()]
    var brDWMixers = [AKDryWetMixer(), AKDryWetMixer()]
    
    // Filter Mixer and Decimator
    var preFilterDistortion = [AKTanhDistortion(), AKTanhDistortion()]
    var postFilterDistortion =  AKTanhDistortion()
    var filterMixer = AKMixer()
    var decimator = AKDecimator()
    
    // FX
    var fxNodes = [AKNode(), AKNode()]
    var reverbs = [AKReverb2(), AKReverb2()]
    var phasers = [AKPhaser(), AKPhaser()]
    var delays = [AKDelay(), AKDelay()]
    var panner = AKPanner()
    
    // If [x][0] then F1, if [x][1] then F2
    var F1F2Panners = [[AKMixer(), AKMixer()], [AKMixer(), AKMixer()], [AKMixer(), AKMixer()]]
    var postFilterMixer = AKMixer()
    
    var outputMixer = AKMixer()
    
    let waveformArray = [AKTable(.sine), AKTable(.sawtooth), AKTable(.square), AKTable(.triangle)]
    
    init(){
        waveformLoad()
        oscillatorSetup()
        setOSCFilterPanValue(oscillator: 1, panValue: initPreset.oscillatorFilterMix[0])
        setOSCFilterPanValue(oscillator: 2, panValue: initPreset.oscillatorFilterMix[1])
        setOSCFilterPanValue(oscillator: 3, panValue: initPreset.oscillatorFilterMix[2])
        preDistortionConnect()
        filtersConnect()
        postFilterMixer = AKMixer(filterNodes[0], filterNodes[1])
        postDistortionConnect()
        decimator = AKDecimator(postFilterDistortion)
        fxConnect()
        panner = AKPanner(fxNodes[1])
        outputMixer = AKMixer(panner)
        /****loadPreset()*****/
        AudioKit.output = outputMixer
        // the last steps
        
        //AudioKit.start()
        
        //(filter as! AKHighPassFilter).cutoffFrequency = 1.0
        
    }

    
    fileprivate func waveformLoad() {
        for oscillators in oscillatorArray {
            for oscillator in oscillators {
                oscillator.waveformArray = waveformArray
            }
        }
    }
    
    fileprivate func preDistortionConnect() {
        for i in 0...1{
            preFilterDistortion[i] = AKTanhDistortion(preFilterMixers[i])
        }
    }
    
    fileprivate func postDistortionConnect() {
        postFilterDistortion = AKTanhDistortion(postFilterDistortion)
    }
    
    
    fileprivate func filtersConnect() {
        hpFilters = [AKHighPassFilter(preFilterDistortion[0]), AKHighPassFilter(preFilterDistortion[1])]
        lpFilters = [AKLowPassFilter(preFilterDistortion[0]), AKLowPassFilter(preFilterDistortion[1])]
        
        bpFilters = [AKBandPassButterworthFilter(preFilterDistortion[0]), AKBandPassButterworthFilter(preFilterDistortion[1])]
        bpDWMixers = [AKDryWetMixer(preFilterDistortion[0],bpFilters[0]), AKDryWetMixer(preFilterDistortion[1],bpFilters[1])]
        
        brFilters = [AKBandRejectButterworthFilter(preFilterDistortion[0]) ,AKBandRejectButterworthFilter(preFilterDistortion[1])]
        brDWMixers = [AKDryWetMixer(preFilterDistortion[0],brFilters[0]), AKDryWetMixer(preFilterDistortion[1],brFilters[1])]
        
        filterNodes[0] = lpFilters[0]
        filterNodes[1] = hpFilters[1]
        
    }
    
    
    fileprivate func oscillatorSetup() {
        oscillatorArray = [OSC1VoiceArray, OSC2VoiceArray, OSC3VoiceArray]
        oscillatorMixers = [OSC1Mixer, OSC2Mixer, OSC3Mixer]
        F1F2Panners = [[oscillatorMixers[0],oscillatorMixers[0]], [oscillatorMixers[1],oscillatorMixers[1]], [oscillatorMixers[2],oscillatorMixers[2]]]
        preFilterMixers = [AKMixer(F1F2Panners[0][0], F1F2Panners[1][0], F1F2Panners[2][1]), AKMixer(F1F2Panners[0][1], F1F2Panners[1][1], F1F2Panners[2][1])]        
    }
    
    fileprivate func fxConnect() {
        phasers[0] = AKPhaser(decimator)
        fxNodes[0] = phasers[0]
        reverbs = [AKReverb2(decimator), AKReverb2(fxNodes[0])]
        delays = [AKDelay(decimator), AKDelay(fxNodes[0])]
        phasers[1] = AKPhaser(fxNodes[0])
        fxNodes[1] = reverbs[1]
    }
    
    
    public func loadPreset(preset: preset!){
        oscillatorPresetLoad(preset: preset)
    }
    
    public func setReverbRoomSize(unit: Int, size: Double) {
        (fxNodes[unit] as! AKReverb2).minDelayTime = size*0.5 + 0.0001
        (fxNodes[unit] as! AKReverb2).maxDelayTime = size*0.9999 + 0.0001
        (fxNodes[unit] as! AKReverb2).decayTimeAtNyquist = size*19.999 + 0.0001
        //(fxNodes[unit] as! AKReverb2).gain = size*6
    }
    
    public func oscillatorPresetLoad(preset: preset!) {
        let currentMixer = AKMixer()
        var j = 0
        for oscillators in oscillatorArray {
            var i = 0
            let spreadDifference = preset.voiceSpread*2/Double(preset.voiceNumber - 1)
            let thisMixer = AKMixer()
            // For each voice
            for oscillator in oscillators {
                let currentDetune = preset.voiceSpread - (spreadDifference * i)
                oscillator.index = preset.oscillatorWaveform[j]
                oscillator.attackDuration = preset.oscillatorAttack[j]
                oscillator.decayDuration = preset.oscillatorDecay[j]
                oscillator.sustainLevel = preset.oscillatorSustain[j]
                oscillator.releaseDuration = preset.oscillatorRelease[j]
                if preset.voiceNumber == 1 {
                    oscillator.pitchBend = preset.oscillatorDetune[j]
                }
                if preset.voiceNumber > 1 {
                    oscillator.pitchBend = preset.oscillatorDetune[j] + currentDetune
                }
                oscillator.vibratoRate = preset.vibratoRate
                oscillator.vibratoDepth = preset.vibratoDepth
                thisMixer.connect(input: oscillator)
                thisMixer.volume = preset.oscillatorVolume[j]
                i += 1
            }
            oscillatorMixers[j] = thisMixer
            j += 1
        }
        voiceMixer = currentMixer
    }
    
    // Function to
    //change the number of oscillator voices
    public func changeVoiceNumber(number: Int, spread: Double) {
        AudioKit.stop()
        currentSettings.voiceNumber = number
        currentSettings.voiceSpread = spread
        var currentOscillatorArray = [[AKMorphingOscillatorBank]]()
        
        // If single voice then no pitch spread - set pitch to detune
        if number == 1 {
            var newArray = oscillatorArray
            for i in 0...2 {
                oscillatorArray[i].removeAll()
            }
            for i in 0...2  {
                let thisMixer = AKMixer()
                let oscillator = newArray[i][0]
                oscillator.pitchBend = currentSettings.oscillatorDetune[i]
                oscillatorArray[i].append(oscillator)
                thisMixer.connect(input: oscillatorArray[i][0])
                oscillatorMixers[i] = thisMixer
            }
        }
        
        // If more than one voice calculate pitch spread and add to mixerbus
        if number > 1 {

            for i in 0...number {
                currentOscillatorArray[i] = oscillatorArray[i]
                oscillatorArray.removeAll()
            }
            
            oscillatorArray = currentOscillatorArray
            
            let spreadDifference = spread*2/Double(number - 1)
            
            var j = 0
            for oscillators in oscillatorArray {
                var i = 0
                let thisMixer = AKMixer()
                for oscillator in oscillators {
                    let currentDetune = spread - (spreadDifference * i)
                    oscillator.pitchBend = currentSettings.oscillatorDetune[j] + currentDetune
                    i += 1
                    thisMixer.connect(input: oscillator)
                }
                j += 1
                oscillatorMixers[j] = thisMixer
            }
        }
        AudioKit.start()
    }
   
    // Oscillators - Integer options = 1, 2, 3
    // When pan is 1 F1 is full mix, when pan is 0 F2 is full mix
    public func setOSCFilterPanValue(oscillator: Int, panValue: Double) {
        switch oscillator {
        case 1:
            F1F2Panners[0][0].volume = panValue
            F1F2Panners[0][1].volume = 1.0 - panValue
            currentSettings.oscillatorFilterMix[0] = panValue
        case 2:
            F1F2Panners[1][0].volume = panValue
            F1F2Panners[1][1].volume = 1.0 - panValue
            currentSettings.oscillatorFilterMix[1] = panValue
        case 3:
            F1F2Panners[2][0].volume = panValue
            F1F2Panners[2][1].volume = 1.0 - panValue
            currentSettings.oscillatorFilterMix[2] = panValue
        default:
            return
        }
    }
    
    public func changeFilterType(filter: Int, type: String) {
        let lastFilter = currentSettings.filterType[filter]
        switch lastFilter {
        case "hpf":
            hpFilters[filter].stop()
        case "lpf":
            lpFilters[filter].stop()
        case "bpf":
            bpFilters[filter].stop()
        case "brf":
            brFilters[filter].stop()
        default:
            return
        }
        
        
        switch type {
        case "hpf":
            hpFilters[filter].cutoffFrequency = currentSettings.filterCutoff[filter]
            hpFilters[filter].resonance = currentSettings.filterResonance[filter]
            hpFilters[filter].dryWetMix = currentSettings.filterDryWet[filter]
            currentSettings.filterType[filter] = "hpf"
            if currentSettings.filterState[filter] == true {
                hpFilters[filter].start()
            }
            filterNodes[filter] = hpFilters[filter]
        case "lpf":
            lpFilters[filter].cutoffFrequency = currentSettings.filterCutoff[filter]
            lpFilters[filter].resonance = currentSettings.filterResonance[filter]
            lpFilters[filter].dryWetMix = currentSettings.filterDryWet[filter]
            currentSettings.filterType[filter] = "lpf"
            if currentSettings.filterState[filter] == true {
                lpFilters[filter].start()
            }
            filterNodes[filter] = lpFilters[filter]
        case "bpf":
            bpFilters[filter].centerFrequency = currentSettings.filterCutoff[filter]
            bpFilters[filter].bandwidth = currentSettings.filterResonance[filter]
            bpDWMixers[filter].balance = currentSettings.filterDryWet[filter]
            currentSettings.filterType[filter] = "bpf"
            if currentSettings.filterState[filter] == true {
                bpFilters[filter].start()
            }
            filterNodes[filter] = bpDWMixers[filter]
        case "brf":
            brFilters[filter].centerFrequency = currentSettings.filterCutoff[filter]
            brFilters[filter].bandwidth = currentSettings.filterResonance[filter]
            brDWMixers[filter].balance = currentSettings.filterDryWet[filter]
            currentSettings.filterType[filter] = "brf"
            if currentSettings.filterState[filter] == true {
                brFilters[filter].start()
            }
            filterNodes[filter] = brDWMixers[filter]
        default:
            return
        }
        
    }
    
    public func changeFXType(fx: Int, newType: String) {
        let lastFX = currentSettings.fxType[fx]
        switch lastFX {
        case "phaser":
            phasers[fx].feedback = currentSettings.fxParam1[fx]
            phasers[fx].lfoBPM = currentSettings.fxParam2[fx]*336 + 24
            phasers[fx].stop()
        case "delay":
            delays[fx].stop()
        case "reverb":
            reverbs[fx].stop()
        default:
            return
        }
        switch newType {
        case "phaser":
            fxNodes[fx] = phasers[fx]
        case "reverb":
            fxNodes[fx] = reverbs[fx]
        case "delay":
            fxNodes[fx] = delays[fx]
        default:
            return
        }
    }
    
    public func playNote(note: MIDINoteNumber) {
        var i = 0
        for oscillators in oscillatorArray {
            i = 0
            for oscillator in oscillators {
                oscillator.play(noteNumber: note + currentSettings.oscillatorOctave[i]*12, velocity: 100)
                i += 1
            }
        }
    }
    
    public func stopNote(note: MIDINoteNumber) {
        var i = 0
        for oscillators in oscillatorArray {
            for oscillator in oscillators {
                oscillator.stop(noteNumber: note + currentSettings.oscillatorOctave[i]*12)
            }
            i += 1
        }
    }
    
    public func startEngine() {
        AudioKit.start()
    }
    
}
