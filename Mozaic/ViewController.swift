//
//  ViewController.swift
//  Mozaic
//
//  Created by William Ravenscroft on 10/10/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // button board arrays
    var keyboard: [UIButton]!
    var scaleboard: [UIButton]!
    var rootboard: [UIButton]!
    var octaveboard: [UIButton]!
    
    // keyboard variables
    @IBOutlet weak var tonicOutlet: UIButton!
    @IBOutlet weak var superTonicOutlet: UIButton!
    @IBOutlet weak var mediantOutlet: UIButton!
    @IBOutlet weak var subDominantOutlet: UIButton!
    @IBOutlet weak var dominantOutlet: UIButton!
    @IBOutlet weak var subMediantOutlet: UIButton!
    @IBOutlet weak var leadingNoteOutlet: UIButton!
    @IBOutlet weak var tonicOctaveOutlet: UIButton!
    
    //????
    // rootboard variables
    @IBOutlet weak var cOutlet: UIButton!
    @IBOutlet weak var cSharpOutlet: UIButton!
    @IBOutlet weak var dOutlet: UIButton!
    @IBOutlet weak var dSharpOutlet: UIButton!
    @IBOutlet weak var eOutlet: UIButton!
    @IBOutlet weak var fOutlet: UIButton!
    @IBOutlet weak var fSharpOutlet: UIButton!
    @IBOutlet weak var gOutlet: UIButton!
    @IBOutlet weak var gSharpOutlet: UIButton!
    @IBOutlet weak var aOutlet: UIButton!
    @IBOutlet weak var aSharpOutlet: UIButton!
    @IBOutlet weak var bOutlet: UIButton!
    
    
    // scaleboard variables
    @IBOutlet weak var majorOutlet: UIButton!
    @IBOutlet weak var naturalOutlet: UIButton!
    @IBOutlet weak var harmonicOutlet: UIButton!
    
    // octaveboard variables
    @IBOutlet weak var zeroOutlet: UIButton!
    @IBOutlet weak var oneOutlet: UIButton!
    @IBOutlet weak var twoOutlet: UIButton!
    @IBOutlet weak var threeOutlet: UIButton!
    @IBOutlet weak var fourOutlet: UIButton!
    @IBOutlet weak var fiveOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        createButtonStructure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func createButtonStructure() {
        // segregated button arrays
        keyboard = [tonicOutlet, superTonicOutlet, mediantOutlet, subDominantOutlet, dominantOutlet, subMediantOutlet, leadingNoteOutlet, tonicOctaveOutlet]
        
        scaleboard = [majorOutlet, naturalOutlet, harmonicOutlet]
        
        rootboard = [cOutlet, cSharpOutlet, dOutlet, dSharpOutlet, eOutlet, fOutlet, fSharpOutlet, gOutlet, gSharpOutlet, aOutlet, aSharpOutlet, bOutlet]
        
        octaveboard = [zeroOutlet, oneOutlet, twoOutlet, threeOutlet, fourOutlet, fiveOutlet]
    }
    
    // keyboard presses
    @IBAction func tonicPressDown(_ sender: Any) {
    }
    @IBAction func superTonicPressDown(_ sender: Any) {
    }
    @IBAction func mediantPressDown(_ sender: Any) {
    }
    @IBAction func subDominantPressDown(_ sender: Any) {
    }
    @IBAction func dominantPressDown(_ sender: Any) {
    }
    @IBAction func subMediantPressDown(_ sender: Any) {
    }
    @IBAction func leadingNotePressDown(_ sender: Any) {
    }
    @IBAction func tonicOctavePressDown(_ sender: Any) {
    }
    
    //scale board press
    
    
    
}

