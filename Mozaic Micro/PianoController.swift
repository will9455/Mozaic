//
//  PianoController.swift
//  Mozaic Keys
//
//  Created by William Ravenscroft on 14/11/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//

import Foundation
import AudioKit

class PianoController {
    
    var oscillator: AKMorphingOscillatorBank
    var mixer: AKMixer!
    let waveformArray = [AKTable(.square), AKTable(.sawtooth), AKTable(.square), AKTable(.sawtooth)]

    init() {
        oscillator = AKMorphingOscillatorBank.init(
            waveformArray: waveformArray,
            index: 0,
            attackDuration: 0.1,
            decayDuration: 0.1,
            sustainLevel: 1.0,
            releaseDuration: 0.3,
            pitchBend: 0,
            vibratoDepth: 0,
            vibratoRate: 0)
        
        oscillator.rampTime = 0.001
        mixer = AKMixer(oscillator)
        mixer.volume = 1.0
        AudioKit.output = mixer
        AudioKit.start()
    }
    
    fileprivate func midiToFrequency(midi: Int) -> Double {
        var frequency = 0.0
        frequency = (440 / 32) * (2 ^ ((midi - 9) / 12))
        return frequency
    }
    
    public func playNote(midi: UInt8, amplitude: Double) {
        oscillator.play(noteNumber: midi, velocity: MIDIVelocity(amplitude*127))
        print(oscillator.index)
    }
    
    public func stopNote(midi: UInt8) {
        oscillator.stop(noteNumber: midi)
    }
    
    public func setVolume(volume: Double) {
        mixer.volume = volume
    }
    
    public func AudioKitOutput() -> AnyObject {
        return AudioKit.output!
    }
}

