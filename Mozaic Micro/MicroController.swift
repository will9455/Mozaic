//
//  ViewController.swift
//  Mozaic Micro
//
//  Created by William Ravenscroft on 15/11/2017.
//  Copyright © 2017 Zypher Audio. All rights reserved.
//
import UIKit

class MicroController: UIViewController {
    
    @IBOutlet weak var buttonOne: UIButton!
    var pianoController = PianoController.init()
    var touchAmplitude = 0.5
    
    // played note = 12 * octave + 24 + rootNote + scaleVector[degree_of_the_scale]
    // scale vectors
    var majorVector = [0, 2, 4, 5, 7, 9, 11, 12]
    var harmonicVector = [0, 2, 3, 5, 7, 8, 11, 12]
    var naturalVector = [0, 2, 3, 5, 7, 8, 10, 12]
    var scaleVector = [0, 2, 4, 5, 7, 9, 11, 12] //starts C Major
    var minorVector = [0, 2, 3, 5, 7, 8, 10, 12] //natural minor to begin with
    // c = 0, c# = 1, d = 2 etc... up to b = 11
    var rootNoteMidiReference = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    var rootNote = 0
    var octave = 3
    var naturalHarmonic = "natural"
    var majorMinor = "major"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tonicPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[0]), amplitude: touchAmplitude)
    }
    @IBAction func superTonicPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[1]), amplitude: touchAmplitude)
    }
    
    @IBAction func mediantPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[2]), amplitude: touchAmplitude)
    }
    @IBAction func subDominantPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[3]), amplitude: touchAmplitude)
    }
    @IBAction func dominantPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[4]), amplitude: touchAmplitude)
    }
    @IBAction func subMediantPress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
            
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[5]), amplitude: touchAmplitude)
    }
    @IBAction func leadingNotePress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
            
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[6]), amplitude: touchAmplitude)
    }
    @IBAction func tonicOctavePress(_ sender: Any, forEvent event: UIEvent) {
        let buttonView = sender as! UIView;
        let touches = event.touches(for: buttonView)
        // get any touch on the buttonView
        if let touch = touches?.first {
            // print the touch location on the button
            let height = Double(buttonOne.bounds.height)
            let yVal = Double(touch.location(in: buttonView).y)
            touchAmplitude = (height - yVal)/height * 0.99 + 0.01
            
        }
        pianoController.playNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[7]), amplitude: touchAmplitude)
    }
    @IBAction func tonicRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[0]))
    }
    
    @IBAction func superTonicRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[1]))
    }
    
    @IBAction func mediantRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[2]))
    }
    @IBAction func subDominantRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[3]))
    }
    @IBAction func dominantRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[4]))
    }
    @IBAction func subMediantRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[5]))
    }
    @IBAction func leadingNoteRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[6]))
    }
    @IBAction func tonicOctaveRelease(_ sender: Any) {
        pianoController.stopNote(midi: UInt8(12 * octave + 24 + rootNote + scaleVector[7]))
    }
    
    @IBAction func dominantKeyPress(_ sender: Any) {
        rootNote = rootNote - 5
    }
    @IBAction func subDominantKeyPress(_ sender: Any) {
        rootNote = rootNote + 5
    }
    @IBAction func relativeKeyPress(_ sender: Any) {
        if majorMinor == "major" {
             rootNote = rootNote - 3
            if naturalHarmonic == "natural" {
                scaleVector = naturalVector
            } else if naturalHarmonic == "harmonic" {
                scaleVector = harmonicVector
            }
            majorMinor = "minor"
        } else if majorMinor == "minor" {
            rootNote = rootNote + 3
            scaleVector = majorVector
            majorMinor = "major"
        }
    }
    
}


